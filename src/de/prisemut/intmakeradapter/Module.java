package de.prisemut.intmakeradapter;

import org.bukkit.entity.Player;

import de.prisemut.intapi.ModuleInterface;
import de.prisemut.intmaker.interrior.SchematicManager;
import de.prisemut.intmaker.interrior.math.listener.PositionListener;
import de.prisemut.intmaker.utils.Cuboid;

public class Module implements ModuleInterface {

	@Override
	public void make(Player player, String style, String room, String direction) {
		/*
		 * Here you can create your module, be creative and enjoy :D
		 * Also here`s an little test module!
		 */
		switch (room) {
		case "livingroom":
			/*
			 * Creating cuboid for interior!
			 */
			Cuboid c = new Cuboid(PositionListener.loc1.get(player), PositionListener.loc2.get(player));

			/*
			 * X equal to Z
			 */
			if (c.getSizeX() == c.getSizeZ()) {
				/*
				 * 5 is odd so there must be a middle!
				 */
				if (c.getSizeX() == 5) {
					/*
					 * Pasting schematic with the schematicmanager!
					 */
					SchematicManager.pasteSchematic(c.getCenter().add(-2, 0, 0), style + "_" + room + "_table");
				}
			}

			break;

		default:
			break;
		}
	}

	@Override
	public String name() {
		/*
		 * Returning the name of the module
		 */
		return "vM1";
	}

}
