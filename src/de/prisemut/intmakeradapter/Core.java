package de.prisemut.intmakeradapter;

import org.bukkit.plugin.java.JavaPlugin;

import de.prisemut.intapi.manager.ModuleManager;

public class Core extends JavaPlugin{

	@Override
	public void onEnable() {
		
		/*
		 * Registration of the module.
		 */
		ModuleManager.registerNewModule(new Module(), "vTest");
		
	}
}
